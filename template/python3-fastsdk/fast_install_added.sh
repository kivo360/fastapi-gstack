#!/bin/bash

export NUM_CORES = $(nproc --all)
pip install -r requirements.txt --install-option="--jobs=$NUM_CORES" --extra-index-url https://__token__:${ACCESS_TOKEN}@${SHAVED_URL}