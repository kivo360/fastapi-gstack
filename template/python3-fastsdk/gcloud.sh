#!/bin/bash

echo "${GCR_MAX}" | base64 -d > master-key.json
gcloud auth activate-service-account --key-file master-key.json
rm master-key.json

gcloud projects list
gcloud config set project ${PROJECT_ID}
gsutil cp gs://dev-tooling/cloud_sql_proxy . && chmod +x ./cloud_sql_proxy

